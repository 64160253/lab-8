package com.warathip.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MapTest {
    @Test
    public void ShouldCorrectHeight1(){
        Map map = new Map();
        assertEquals(5, map.getHeight());
    }

    @Test
    public void ShouldCorrectWidth1(){
        Map map = new Map();
        assertEquals(5, map.getWidth());
    }

    
    @Test
    public void ShouldCorrectHeight2(){
        Map map = new Map(10,20);
        assertEquals(20, map.getHeight());
    }

    public void ShouldCorrectWidth2(){
        Map map = new Map(30,40);
        assertEquals(30, map.getWidth());
    }
}
